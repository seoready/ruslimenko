$(document).ready(function() {

    $.stellar();

    $('.fancybox').fancybox();

    $('a[href^="#"]').click(function() {
        var el = $(this).attr('href');
        $('body').animate({
            scrollTop: $(el).offset().top
        }, 1000);
        return false;
    });


    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        slidesPerColumn: 2,
        paginationClickable: true,
        autoplay: 3000,
        spaceBetween: 14
    });

    var swiper_cube = new Swiper('.swiper_cube_conteiner', {
        // pagination: '.swiper-pagination',
        effect: 'cube',
        grabCursor: true,
        nextButton: '.swiper_cube-button-next',
        prevButton: '.swiper_cube-button-prev',
        autoplay: 6000,
        cube: {
            shadow: false,
            slideShadows: false,
            shadowOffset: 10,
            shadowScale: 0.8
        }
    });

    // Elevator script included on the page, already.

    window.onload = function() {
        var elevator = new Elevator({
            element: document.querySelector('.elevator-button'),
            mainAudio: './upper.mp3',
            endAudio: './elevator.mp3'
        });
    };


    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.elevator-button').fadeIn();
        } else {
            $('.elevator-button').fadeOut();
        }
    });

});
